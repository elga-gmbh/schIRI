package tanjga.eu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.helger.schematron.ISchematronResource;
import com.helger.schematron.pure.SchematronResourcePure;
import com.helger.schematron.xslt.ISchematronXSLTBasedProvider;
import com.helger.schematron.xslt.SchematronResourceSCH;
import com.helger.schematron.xslt.SchematronResourceXSLT;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class App {
    private App() {
    }

    // This is a optimized parameter, that should not be handled by the user/arguments
    private final static int maxPatternInPhases = 50;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        //File aSchematronFile = new File("C:\\Users\\ntanjga.ELGA\\Downloads\\cda-telemonitoring-episodenbericht-schematron-master\\cda-telemonitoring-episodenbericht-schematron-master\\elgatgd-Telemonotoring-Episodenbericht.sch");
        //File aSchematronFile = new File("C:\\Users\\ntanjga.ELGA\\OneDrive - Elga\\sch\\telemonitoring-episodenbericht-schematron\\elgatgd-Telemonotoring-Episodenbericht.sch");
        //File anotherSchematronFile = new File("C:\\Users\\ntanjga.ELGA\\OneDrive - Elga\\sch\\ambulanzbefund-schematron\\elgaambbef-Ambulanzbefund.sch");

        ArrayList<File> fArr = new ArrayList<File>();

        for (int i = 0; i < args.length; i++) {
            if (args[i].endsWith(".sch"))
                fArr.add(new File(args[i]));
        }

        if (fArr.size() > 1)
            createTheIntersectionsAndOverlaps(fArr, Integer.parseInt(args[0]));
        else {
            //File aXMLFile = new File("C:\\Users\\ntanjga.ELGA\\Downloads\\TmE_Dummy_Fall_Haeckler_v3.2_maximal.xml");
            //File aSecondXMLFile = new File("C:\\Users\\ntanjga.ELGA\\OneDrive - Elga\\cda-Beispielbefund\\Telemonitoring-Episodenbericht\\TmE_Dummy_Fall_Haeckler_v3.2_minimal.xml");
            //File aSchematronXSL = new File("C:\\Users\\ntanjga.ELGA\\OneDrive - Elga\\OVP\\schPure\\TmE_Amb_Intersection_2021-06-08.xsl");

            long startTime;
            long endTime;

            try {
                File file = fArr.get(0);
                String[] sArr = file.getPath().split("\\\\");
                String schName = sArr[sArr.length-1].split("\\.")[0];

                // read in all phases from this file
                ArrayList<String> phaseList = readPhasesFromFile(file);

                // looking for the right phases fitting the regex
                Set<String> phasesToGenerate = new HashSet<String>();
                for (String s : args)
                    if (!s.endsWith(".sch"))
                        for (String sPhase : phaseList)
                            if (sPhase.matches(s))
                                phasesToGenerate.add(sPhase);

                // generate the phases to xslt
                for (String phase : phasesToGenerate){
                    startTime = System.currentTimeMillis();
                    exportXSL(file, phase, schName);
                    endTime = System.currentTimeMillis();
                    System.out.println("  done in "+new SimpleDateFormat("mm:ss.SSS").format(endTime - startTime));
                }

                //System.out.println(validateXMLViaXSLT(aSchematronXSL, aXMLFile));
                //System.out.println(validateXMLViaXSLTSchematronAndExportXSL(aSchematronFile, aXMLFile, "Main"));

                //System.out.println(new SimpleDateFormat("mm:ss.SSS").format(endTime - startTime));

                //System.console().readLine();
                //startTime = System.currentTimeMillis();

                //System.out.println(validateXMLViaXSLT(aSchematronXSL, aXMLFile));
                //System.out.println(validateXMLViaXSLTSchematronAndExportXSL(aSchematronFile, aXMLFile, "Intersection"));
                //endTime = System.currentTimeMillis();
                //System.out.println(new SimpleDateFormat("mm:ss.SSS").format(endTime - startTime));
            } catch (Exception e) {
                System.err.println(e.toString());
            }
            //System.out.println();
            //System.console().readLine();
        }
    }

    private static ArrayList<String> readPhasesFromFile(File file) throws FileNotFoundException {
        Scanner myReader = new Scanner(file);
        String data = "";
        ArrayList<String> phaseList = new ArrayList<String>();
        while (myReader.hasNextLine()){
            data = myReader.nextLine().trim();
            if (data.matches("<phase id=.*>"))
                phaseList.add(data.substring(11, data.length()-2));
        }
        myReader.close();
        return phaseList;
    }

    private static String readInfoPerRegex(File file, String regex, int beginIndex, int endIndex) throws FileNotFoundException {
        Scanner myReader = new Scanner(file);
        String data = "";
        while (myReader.hasNextLine()){
            data = myReader.nextLine().trim();
            if (data.matches(regex)){
                myReader.close();
                if (endIndex <= 0)
                    return data.substring(beginIndex, data.length()+endIndex);
                else
                    return data.substring(beginIndex, endIndex);
            }
        }
        myReader.close();
        return null;
    }

    public static void createTheIntersectionsAndOverlaps(ArrayList<File> InputSchematrons, int minPatternsForOverlap) throws FileNotFoundException, IOException{
        /** extracting the patterns out of the schematrons */
        ArrayList<ArrayList<String>> inSchematronPattern = new ArrayList<ArrayList<String>>();
        // open every File
        for (File oneFile : InputSchematrons) {
            Scanner myReader = new Scanner(oneFile);
            // make a List for found patterns in the file
            ArrayList<String> pattern = new ArrayList<String>();
            // adding the name of the file/schematron/.sch as first string in the array
            pattern.add(oneFile.getName().split("\\.")[0]);
            // creating a temp
            String data = "";
            // go through file till found searched phase
            while (myReader.hasNextLine() && !data.matches("<phase id=\"AllExceptClosed\">")){
                data = myReader.nextLine().trim();
            }
            // when the searched phase is found, store every pattern in a arraylist
            while (myReader.hasNextLine() && !data.matches("<\\/phase>")){
                data = myReader.nextLine().trim();
                if(data.matches(".*pattern.*")){
                    pattern.add(data);
                }
            }
            //add the finished pattern set of the file to the storeage
            inSchematronPattern.add(pattern);
        }

        /** add to every schematron the main phase, build out of AllExceptClose and the DLTClosed */
        int i = 0;
        for (File oneFile : InputSchematrons) {
            // create the closed pattern of the first pattern / normally the DLT
            String sClosedPattern = inSchematronPattern.get(i).get(1);
            sClosedPattern = sClosedPattern.substring(0, sClosedPattern.length()-3);
            sClosedPattern += "-closed\"/>";
            // add the closed pattern and create the phase
            inSchematronPattern.get(i).add(sClosedPattern);
            addPhaseToFile("Main",oneFile,inSchematronPattern.get(i));
            i++;
        }

        /** find the intersection of all schematrons */
        ArrayList<String> intersectionPatterns = new ArrayList<String>(inSchematronPattern.get(0));
        // go through every list
        for (ArrayList<String> arrayList : inSchematronPattern) {
            // keep all Elements in the ArrayLists that are in both collections
            intersectionPatterns.retainAll(arrayList);
        }

        // are there enough includes in the phase
        if (intersectionPatterns.size() >= minPatternsForOverlap){
            // add to every schematron the intersection phase
            for (File oneFile : InputSchematrons) {
                addPhaseToFile("Intersection",oneFile,intersectionPatterns);
            }

            /** add to every schematron a schema AllExceptIntersection */
            i = 0;
            for (File oneFile : InputSchematrons) {
                // remove all patterns that are in the intersection
                ArrayList<String> allExceptIntersection = new ArrayList<String>(inSchematronPattern.get(i));
                allExceptIntersection.removeAll(intersectionPatterns);
                addPhaseToFile("AllExceptIntersection",oneFile,allExceptIntersection);
                i++;
            }
        }


        /** Find the overlap of all sets of schematrons */

        /**
         * A powerset/powerlist    (third dimension, all combinations of the lists from zero to all lists inside https://en.wikipedia.org/wiki/Power_set)
         * with multiple lists of  (second dimension, the whole lists)
         * all patterns/includes.  (first dimension, the patterns from one list)
         * */
        ArrayList<ArrayList<ArrayList<String>>> powerSetOfPattern = powerSet(inSchematronPattern);

        ArrayList<ArrayList<String>> allOverlapPatterns = new ArrayList<ArrayList<String>>();
        allOverlapPatterns.add(intersectionPatterns);
        // go through every set in the powerset, first take the biggest ones and get smaller
        for (int powerSetSizeToProcess = InputSchematrons.size()-1; powerSetSizeToProcess > 1; powerSetSizeToProcess--)
            for (ArrayList<ArrayList<String>> oneSetOfPowerSet : powerSetOfPattern){
                if(oneSetOfPowerSet.size() == powerSetSizeToProcess){
                    // create a new overlapPatterns and fill it with the first set
                    ArrayList<String> theOverlapPatterns = new ArrayList<String>(oneSetOfPowerSet.get(0));
                    // first intersect the given patterns
                    for (ArrayList<String> arrayList : oneSetOfPowerSet) {
                        // keep all Elements in the ArrayLists that are in all collections
                        theOverlapPatterns.retainAll(arrayList);
                    }
                    // now remove the patterns that are already in the overlaps, even if the overlaps are not related to the new overlap
                    for (ArrayList<String> arrayList : allOverlapPatterns){
                        // delete all Elements in the ArrayLists that are already in the intersection patterns
                        theOverlapPatterns.removeAll(arrayList);
                    }
                    // we do now have a overlap
                    // now decide if the overlap is big enough to be meaningfull
                    if(theOverlapPatterns.size() >= minPatternsForOverlap){
                        // if it is add it to the list of all overlaps
                        allOverlapPatterns.add(theOverlapPatterns);
                        // add the names of the original sch from where the patterns where from to the beginning of the list
                        // and at the same time create the phase name for later
                        String sPhaseWithOverlapSchNames = "OverlapOf";
                        for (ArrayList<String> arrayList : oneSetOfPowerSet){
                            theOverlapPatterns.add(0, arrayList.get(0));
                            sPhaseWithOverlapSchNames += "_"+arrayList.get(0);
                        }
                        // adding the name of the pattern as the first element
                        theOverlapPatterns.add(0, sPhaseWithOverlapSchNames);
                        // and write it into the files/.sch from the overlap
                        for (ArrayList<String> arrayList : oneSetOfPowerSet){
                            // look for the right file
                            for (File oneFile : InputSchematrons) {
                                if (arrayList.get(0).equals(oneFile.getName().split("\\.")[0]))
                                    // and append it to the file
                                    addPhaseToFile(sPhaseWithOverlapSchNames,oneFile,theOverlapPatterns);
                            }
                        }
                    }
                }
            }

        /** add to every schematron a schema AllExceptOverlaps */
        i = 0;
        for (File oneFile : InputSchematrons) {
            // remove all patterns that are in the intersection
            ArrayList<String> allExceptOverlaps = new ArrayList<String>(inSchematronPattern.get(i));
            boolean once = false;
            for(ArrayList<String> arrayList : allOverlapPatterns){
                allExceptOverlaps.removeAll(arrayList);
                once = true;
            }
            if (once)
                addPhaseToFile("AllExceptOverlaps",oneFile,allExceptOverlaps);
            i++;
        }


        System.out.println("The treashold for xslt generation was a minimum of "+minPatternsForOverlap+" pattern in a overlap.");
        System.out.println("For below schematrons the following phases have been generated:");
        /**for (ArrayList<String> arrayList : inSchematronPattern){
            String sOut = arrayList.get(0)+": {Main}";
            if (intersectionPatterns.size() >= minPatternsForOverlap)
                sOut +=  "(AllExceptIntersection [Intersection)";
            else
                sOut += " [";
            boolean firstTime = true;
            for(ArrayList<String> aop : allOverlapPatterns)
                if (aop.contains(arrayList.get(0))){
                    if (firstTime){
                        firstTime = false;
                        sOut += " AllExceptOverlaps";
                    }
                    sOut += " "+aop.get(0);
                }
            sOut += "]";
            System.out.println();
            System.out.println(sOut);
        } */

        /** print out which phases have been created in what .sch */
        for (File oneFile : InputSchematrons){
            String sOut = oneFile.getName().split("\\.")[0]+":";
            for (String s : readPhasesFromFile(oneFile))
                if (s.matches("Main.*") || s.matches("Intersection.*") || s.matches("AllExceptIntersection.*")
                        || s.matches("Overlap.*") || s.matches("AllExceptOverlaps.*"))
                    sOut += " "+s;
            System.out.println();
            System.out.println(sOut);
        }
    }

    public static void addPhaseToFile(String phase, File theFile, ArrayList<String> patterns) throws IOException{
        /** count how many includes/patterns are in a phase, split them if to big */
        if(patterns.size() > maxPatternInPhases){
            addPhaseToFile(phase+"0", theFile, new ArrayList<String>(patterns.subList(0, patterns.size()/2)));
            addPhaseToFile(phase+"1", theFile, new ArrayList<String>(patterns.subList(patterns.size()/2, patterns.size())));
        }else{
            String sOut = new String();

            /** read the whole file and delete line(s) with </schema> in file */
            BufferedReader reader = new BufferedReader(new FileReader(theFile));
            String lineToRemove = "</schema>";
            String currentLine;

            while((currentLine = reader.readLine()) != null) {
                // trim newline when comparing with lineToRemove
                String trimmedLine = currentLine.trim();
                if(trimmedLine.equals(lineToRemove)) continue;
                sOut += currentLine+"\n";
            }
            reader.close();

            /** add the new intersection-phase */
            sOut += "   <phase id=\""+phase+"\">\n";
            for (String string : patterns) {
                if (string.startsWith("<active"))
                    sOut += "      "+string+"\n";
            }
            sOut += "   </phase>\n";
            sOut += "</schema>\n";

            /** create in schematron a new pattern with the intersection */
            Files.write(theFile.toPath(), sOut.getBytes());
        }
    }

    public static boolean validateXMLViaXSLT (@Nonnull final File aSchematronFile, @Nonnull final File aXMLFile) throws Exception
    {
        final ISchematronResource aResSCH = SchematronResourceXSLT.fromFile (aSchematronFile);
        if (!aResSCH.isValidSchematron ())
            throw new IllegalArgumentException ("Invalid Schematron!");
        return aResSCH.getSchematronValidity (new StreamSource(aXMLFile)).isValid ();
    }

    public static boolean validateXMLViaPureSchematron(@Nonnull final File aSchematronFile,
            @Nonnull final File aXMLFile) throws Exception {
        final ISchematronResource aResPure = SchematronResourcePure.fromFile(aSchematronFile);
        if (!aResPure.isValidSchematron())
            throw new IllegalArgumentException("Invalid Schematron!");
        return aResPure.getSchematronValidity(new StreamSource(aXMLFile)).isValid();
    }

    public static boolean validateXMLViaXSLTSchematronAndExportXSL(@Nonnull final File aSchematronFile,
            @Nonnull final File aXMLFile, String phase) throws Exception {
        final SchematronResourceSCH aResSCH = SchematronResourceSCH.fromFile(aSchematronFile);
        if (phase != null){
            aResSCH.setPhase(phase);
            System.out.println("The schematron/.sch file \""+aSchematronFile.getAbsolutePath()+"\" is beeing"+
             "processed with the phase \""+phase+"\" to a xsl file. Depending on your computing power, this can take up to five minutes.");
        }

        if (!aResSCH.isValidSchematron())
            throw new IllegalArgumentException("Invalid Schematron!");

        ISchematronXSLTBasedProvider base = aResSCH.getXSLTProvider();
        Document d = base.getXSLTDocument();

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(d);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("_yyyy-MM-dd");
        FileWriter writer = new FileWriter(new File(phase+dtf.format(LocalDateTime.now())+".xsl"));
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);

        return aResSCH.getSchematronValidity(new StreamSource(aXMLFile)).isValid();

    }

    public static void exportXSL(@Nonnull final File aSchematronFile, String phase, String schName) throws Exception {
        final SchematronResourceSCH aResSCH = SchematronResourceSCH.fromFile(aSchematronFile);

        // if the namePrefix is a OverlapOf, than delete the prefix
        String namePrefix = null;
        if(phase.matches("OverlapOf.*") || phase.matches("Intersection.*")) namePrefix = ""; else namePrefix = schName;

        if (phase != null){
            aResSCH.setPhase(phase);
            System.out.println("creating phase/xsl "+namePrefix+"_"+phase+", please wait...");
        }

        if (!aResSCH.isValidSchematron())
            throw new IllegalArgumentException("Invalid Schematron!");

        ISchematronXSLTBasedProvider base = aResSCH.getXSLTProvider();
        Document d = base.getXSLTDocument();

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(d);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String fname = namePrefix+"_"+phase+"_"+dtf.format(LocalDateTime.now())+".xsl";
        FileWriter writer = new FileWriter(new File(fname));
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);

        /** adapt config xml beside of exporting  */
        File f = new File("schIRI_xsls_2_IG.xml");
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        Document doc = null;
        try {
            docBuilder = docFactory.newDocumentBuilder();
            if (f.isFile()){
                doc = docBuilder.parse(f);
            } else {
                doc = docBuilder.newDocument();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }catch (SAXException e) {
            e.printStackTrace();
        }

        Element rootElement = doc.createElement("schIRI-xsls-2-IG");
        if (f.isFile()){
            rootElement = doc.getDocumentElement();
        } else {
            doc.appendChild(rootElement);
        }

        Element load = doc.createElement("load");
        if (f.isFile()){
            load = (Element) rootElement.getElementsByTagName("load").item(0);
        } else {
            rootElement.appendChild(load);
        }

        // find ig element or add it to root
        Element ig = null;
        NodeList nlAllIG = rootElement.getElementsByTagName("IG");
        for (int i = 0; i < nlAllIG.getLength(); i++)
            if (nlAllIG.item(i).getAttributes().getNamedItem("schName").getNodeValue().equals(schName))
                ig = (Element) nlAllIG.item(i);
        if (ig == null){
            ig = doc.createElement("IG");
            ig.setAttribute("igName", readInfoPerRegex(aSchematronFile, "<title>Schematron file for transaction [\\s\\S]* \\(.*",39,0).split("\\(1\\.2\\.40\\.")[0].trim());
            ig.setAttribute("dltTemplateId", readInfoPerRegex(aSchematronFile, "<active pattern=\"template-.*\"/>",26,-21));
            ig.setAttribute("schVersion", readInfoPerRegex(aSchematronFile, "<!-- versionLabel=\".*\" -->",19,-5)); // not anymore used semver regex: (0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\+\\d{8}-.*
            ig.setAttribute("schName", schName);
            rootElement.appendChild(ig);
        }

        // create and add schIRI-xsl element to ig, if not already there
        boolean foundIt = false;
        NodeList nlSchiri = ig.getElementsByTagName("schIRI-xsl");
        for (int i = 0; i < nlSchiri.getLength(); i++)
            if (nlSchiri.item(i).getTextContent().equals(fname))
                foundIt = true;
        if (!foundIt){
            Element schIRIxsl = doc.createElement("schIRI-xsl");
            schIRIxsl.setTextContent(fname);
            ig.appendChild(schIRIxsl);
        }

        // create and add schIRI-xsl element to load, if not already there
        foundIt = false;
        nlSchiri = load.getElementsByTagName("schIRI-xsl");
        for (int i = 0; i < nlSchiri.getLength(); i++)
            if (nlSchiri.item(i).getTextContent().equals(fname))
                foundIt = true;
        if (!foundIt){
            Element schIRIxsl = doc.createElement("schIRI-xsl");
            schIRIxsl.setTextContent(fname);
            load.appendChild(schIRIxsl);
        }

        // write the content into xml file
        try {
            TransformerFactory transformerFactory2 =  TransformerFactory.newInstance();
            Transformer transformer2;

            transformer2 = transformerFactory2.newTransformer();
            transformer2.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source2 = new DOMSource(doc);

            StreamResult result2 =  new StreamResult(f);
            transformer2.transform(source2, result2);
            System.out.println("  added phase/xsl to load and "+schName+" in schIRI_xsls_2_IG.xml");
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
/*
    public static void createXSLTFromSCH(@Nonnull final IReadableResource aSchematronResource,
            @Nonnull final SCHTransformerCustomizer aTransformerCustomizer, String phase) throws Exception {
        final SchematronResourceSCH aResSCH = SchematronResourceSCH.fromFile(aSchematronFile);
        //String phase = null;
        //phase = "AllExceptClosed";
        //phase = "OnlyClosed";
        //phase = "myPhase";
        if (phase != null){
            aResSCH.setPhase(phase);
            System.out.println(phase);
        }

        if (!aResSCH.isValidSchematron())
            throw new IllegalArgumentException("Invalid Schematron!");
        return aResSCH.getSchematronValidity(new StreamSource(aXMLFile)).isValid();
    }*/


    /**
     * Slightly changes from https://www.howtobuildsoftware.com/index.php/how-do/blK6/java-set-set-intersection-set-theory-how-to-find-the-list-of-all-intersections-of-multiple-sets-in-java
     * Creating a powerset
     * @param <T> the type of the elements
     * @param originalSet the original set/list with elements
     * @return the two dimensional powerset/powerlist with alle their elements
     */
    public static <T> ArrayList<ArrayList<T>> powerSet(ArrayList<T> originalSet) {
        ArrayList<ArrayList<T>> sets = new ArrayList<ArrayList<T>>();
        if (originalSet.isEmpty()) {
            sets.add(new ArrayList<T>());
            return sets;
        }
        List<T> list = new ArrayList<T>(originalSet);
        T head = list.get(0);
        ArrayList<T> rest = new ArrayList<T>(list.subList(1, list.size()));
        for (ArrayList<T> set: powerSet(rest)) {
            ArrayList<T> newSet = new ArrayList<T>();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }
}

# schIRI - Schemtron Intersection for RAM Improvement

[[_TOC_]]

## Where to download
https://gitlab.com/elga-gmbh/schIRI/-/blob/master/schIRI/target/schIRI-1.2.1.jar

## How to use

There are two different modes the application can be used for, one will create phases out of given phases an their includes and the other will create xsl out of phases. 

### Creating intersection- & overlap-phases for the xsl
    java -jar schIRI-1.2.1.jar smallI schematronNameOne.sch schematronNameTwo.sch [... schematronNameSmallN.sch]

* for creating one intersection and one to many overlaps with a minimum of *i* (positive number) includes of schematronNameOne.sch, schematronNameTwo.sch to *n* schematrons if needed (*n* is the number of schematrons/.sch)
* minimum two schematrons/.sch-Files needed to be stated in the arguments
* will create following 2^*n*+1 new phases in all given schematrons/.sch
  * A "Intersection"-phase including all includes in all stated schematrons,  
  * A "AllExceptIntersection"-phase, that is complete with the Intersection-phase.
  * A "Main"-phase, that is complete itself.
  * Multiple "_OverlapOf_[schematronNameNumber]_[schematronNameAnotherNumber]_..."-phases. There are *n*^2/2-2 overlaps per schematron/.sch .
  * A "AllExceptOverlaps"-phase, that is complete with all other _OverlapOf_*-phases.

### Creating a xslt from specific phases
    java -jar schIRI-1.2.1.jar sch1.sch thisPhaseOrPhasesFittingTheRegex [... thatPhaseOrPhasesFittingTheRegex]

* for creating a xslt out of all phases in sch1.sch with the phase-name "thisPhaseOrPhasesFittingTheRegex" and any other phase that is given
* please keep in mind that regex need to mask some characters, if you want to use them as text
* minimum one phase name is required
* one regular expression can include more than one phase, all of these will be generated to a xsl
* while creating the xslt it also creates or appends to a exisiting config file in the same folder named "schIRI_xsls_2_IG.xml"

### How is it used for ELGA

First, copy schIRI-1.2.1.jar into the "cda-gesamt-schematron-master"-Folder, downloaded from here: https://gitlab.com/elga-gmbh/cda-schematron/cda-gesamt-schematron .

_Notice: File encoding has to be set with "-Dfile.encoding=UTF-8", if not set globally. Also the JVM size should be set for generating the bigger xsl's to -Xmx8192m for 8GB of memory. See next commands._

First run following, to create the Overlap- and Intersection-Phases with a minimum of 4 includes for all schematrons:

    java "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar 4 .\elgaambbef-Ambulanzbefund.sch .\elga-cdabgd.sch .\elga-cdaea.sch .\elga-cdaemdp.sch .\elga-cdaemml.sch .\elga-cdaempa.sch .\elga-cdaempr.sch .\elga-cdaep.sch .\elga-cdalab.sch .\elga-cdapsb.sch .\elgaimpf-KompletterImmunisierungsstatus.sch .\elgaimpf-UpdateImmunisierungsstatus.sch .\elgatgd-Telemonitoring-Episodenbericht.sch .\at-lab-Laborbefund.sch .\at-lab-Mikrobiologiebefund.sch .\epims-Labormeldung.sch .\epims-Arztmeldung.sch

Then run all of these commands, to create the xsl out of all Overlap-schematrons in the sch-Files:

    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\at-lab-Laborbefund.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\at-lab-Mikrobiologiebefund.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elgaambbef-Ambulanzbefund.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elgatgd-Telemonitoring-Episodenbericht.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdabgd.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdaea.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdaemdp.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdaemml.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdaempa.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdaempr.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdaep.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdalab.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elga-cdapsb.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\elgaimpf-KompletterImmunisierungsstatus.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar  .\elgaimpf-UpdateImmunisierungsstatus.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\epims-Arztmeldung.sch .*verlap.*
    java -Xmx8192m "-Dfile.encoding=UTF-8" -jar schIRI-1.2.1.jar .\epims-Labormeldung.sch .*verlap.*

The file names of the xsl are named after the Overlap-phases, the Overlap-phases are named after the original phases that are overlapping.

## What does it do
By generating xslt's out of many schematrons/.sch with the same includes, these same includes are not beeing considered in the generation process. The most projects overlap in at least some includes, so the resulting xslt have many redundand includes. Big xslt/schematrons can consume more than 8GB memory, checking a xml. As an example, overlapping with 33% with five schematrons would mean a redundancy and additional need of memory of 26% (after the first schematron additional 33%). Overlapping with 66% with 10 schematrons would result in 59% additional memory need!

With schIRI, redundancies are beeing considerd and the one big schematron is beeing splitted into many, so that also a serial processing with smaller xslt is possible on a computer with less than 8GB free memory.
Following graphs are explaining the problem and schIRI as a solution visually:
![visual_exampel_with_three_schematrons](visual_exampel_with_three_schematrons.png "visual_exampel_with_three_schematrons")

Here another exampel with four schematrons, every schematron is again one bit in a, this time, four digit biniary:
![visual_exampel_with_four_schematrons](visual_exampel_with_four_schematrons.png "visual_exampel_with_four_schematrons")
* The Set with all ones is the Intersection between all of the schematrons.
* The Set with three ones have three of four schematrons included, the intersection has to be excluded.
* The Set with two ones have two of four schematrons included, the intersection and the previous overlaps have to be excluded.
* The Sets with one one are AllExceptOverlap-Phases, where also the intersection and the previous overlaps are excluded.
